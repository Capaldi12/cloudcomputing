# Лабораторная 2

Задание - [Challenge2.md](https://github.com/kirill20123456/cloud-computing-course/blob/main/Lessons/Lesson2/Challenge2.md)

## Парсер Volgograd.ru

В результате выполнения второй лабораторной работы:

- инсталлирован `Docker` на локальной машине
- созданы два `Dockerfile`, один для работы парсера, второй для развертывания СУБД
- создан `docker-compose.yaml` для объединения будущих контейнеров в единую функциональность
- развёрнуты функциональности парсера и СУБД в контейнерах
- тестирование работы функциональности средствами `Docker` успешно завершено
- начата работа над развёртыванием оркестратора `Apache Airflow`

## Веб-интерфейс

В результате выполнения лабораторной работы:

- произведено прототипирование на локальном компьютере
- перенос в `Docker` контейнер
- внедрение контейнера в `docker-compose.yaml`
- реализована кнопка выгрузки статей на сайт в виде таблицы
- протестирована работа веб-интерфейса

## Технологический стек

- Docker
- Apache Airflow 2.2.5
- MongoDB
- Python 3.10
  - Flask
  - PyMongo
  - apispec
- Python 3.8
  - BeautifulSoup
- ReactJS
