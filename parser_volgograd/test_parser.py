import datetime
import requests
from bs4 import BeautifulSoup

# Ссылки на сайт для парсинга

URL = 'http://www.volgograd.ru/news/'
HOST = 'http://www.volgograd.ru'

# Указывается номер страницы новостей на сайте
PARAMS = {}

# Возврат определенной html-страницы сайта новостей
def get_html(url, params):
    return requests.get(url, params=params)

# Парсинг текста новости
def get_news_text(url):
    html = get_html(url, PARAMS)
    if html.status_code != 200:
        return "Error to load"
    soup = BeautifulSoup(html.text, 'html.parser')
    try:
        paragraphs = soup.find('div', class_="news-detail").find_all('p')
    except AttributeError:
        return "err"
    text = ""
    for paragraph in paragraphs:
        text += paragraph.get_text(strip=True)
    return str(text)


# Создание коллекции для наполнения бд
def get_content(html):
    soup = BeautifulSoup(html.text, 'html.parser')
    items = soup.find_all('div', class_='col-md-12 news-item')
    for item in items:
        name = item.find('a').get_text()
        link = HOST + item.find('a').get('href')
        text = get_news_text(link)
        if text == 'Error to load':
            continue
        date = format_date(item.find('div', class_='date').get_text())
        today = str(datetime.date.today())
        if date == today:
            news = {
                'title': name,
                'content': text,
                'url': link,
                'date': date
            }
            send_post_request(news)

def send_post_request(news):
    url = "http://localhost:4242/api/database/articles"
    r = requests.post(url=url, json=news)
    print(r.status_code)
    print(r.text)


def format_date(date):
    month_list = ['января', 'февраля', 'марта', 'апреля', 'мая', 'июня',
                  'июля', 'августа', 'сентября', 'октября', 'ноября', 'декабря']
    date_list = date.split(' ')
    year = int(date_list[2])
    month = month_list.index(str(date_list[1]).lower())+1
    day = int(date_list[0])

    return str(datetime.date(year,month,day))


def parse(num_page):
    PARAMS = {'PAGEN_1': num_page}
    # Получение html страницы новостей
    html = get_html(URL, PARAMS)
    # Если страница новостей получена, то начинаем её парсить
    if html.status_code == 200:
        get_content(html)
    else:
       print('Error')


parse(1)

#Для просмотра записанных записей
r2 = requests.get(url="http://localhost:4242/api/database/articles")
t = r2.text.encode().decode('unicode-escape')
