from datetime import datetime, timedelta
from airflow import DAG
from airflow.operators.python import PythonOperator
import logging

default_args = {
'owner' : 'admin',
'depend_on_past' : False,
'start_date' : datetime(2022, 4, 2, 23),
'retries' : 1,
'retry_delay' : timedelta(minutes=5),
'schedule_interval': '* 23 * * *',
}


with DAG('DAG_VOLGOGRAD_RU', default_args=default_args, catchup=False) as dag:
	
    import datetime
    import requests
    from bs4 import BeautifulSoup
    # Ссылки на сайт для парсинга
    URL = 'http://www.volgograd.ru/news/'
    HOST = 'http://www.volgograd.ru'

    LOGGER = logging.getLogger("task_parse_volgograd_ru")
    # Указывается номер страницы новостей на сайте
    PARAMS = {}

    # Возврат определенной html-страницы сайта новостей
    def get_html(url, params):
        return requests.get(url, params=params)

    # Парсинг текста новости
    def get_news_text(url):
        html = get_html(url, PARAMS)
        if html.status_code != 200:
            return "Error to load"
        soup = BeautifulSoup(html.text, 'html.parser')
        try:
            paragraphs = soup.find('div', class_="news-detail").find_all('p')
        except AttributeError:
            return "err"
        text = ""
        for paragraph in paragraphs:
            text += paragraph.get_text(strip=True)
        return str(text)


    # Создание коллекции для наполнения бд
    def get_content(html):
        soup = BeautifulSoup(html.text, 'html.parser')
        items = soup.find_all('div', class_='col-md-12 news-item')
        for item in items:
            name = item.find('a').get_text()
            link = HOST + item.find('a').get('href')
            text = get_news_text(link)
            if text == 'Error to load':
                continue
            date = format_date(item.find('div', class_='date').get_text())
            today = str(datetime.date.today())
            #TODO : разобраться с кодировкой encode().decode('unicode-escape')
            #TODO : выработать условие для запуска новостей (date == today)
            news = {
                'title': name,
                'content': text,
                'url': link,
                'date': date
            }
            send_post_request(news)

    def send_post_request(news):
        url = "http://database:80/api/database/articles"
        r = requests.post(url=url, json=news)
        LOGGER.info("article added")
        print(r.status_code)


    def format_date(date):
        month_list = ['января', 'февраля', 'марта', 'апреля', 'мая', 'июня',
                  'июля', 'августа', 'сентября', 'октября', 'ноября', 'декабря']
        date_list = date.split(' ')
        year = int(date_list[2])
        month = month_list.index(str(date_list[1]).lower())+1
        day = int(date_list[0])

        return str(datetime.date(year,month,day))

    #for get text need write encode().decode('unicode-escape')

    def parse(**kwargs):
        num_p = kwargs['num_page']
        PARAMS = {'PAGEN_1': num_p}
        # Получение html страницы новостей
        html = get_html(URL, PARAMS)
        LOGGER.info("html has been gotten")
        # Если страница новостей получена, то начинаем её парсить
        if html.status_code == 200:
            get_content(html)
            LOGGER.info("content has been gotten")
        else:
            LOGGER.info("status code != 200")
            
    task = PythonOperator(
        task_id='task_parse_volgograd_ru',
        python_callable=parse,
	op_kwargs={
		'num_page' : 1
	},
	dag=dag
    )

task