from bottle import route, run, response
from parser import main
import threading


thread = None


@route('/api/parser/v1/run')
def run_parser():
    response.set_header('Access-Control-Allow-Origin', '*')

    global thread

    if thread is not None and thread.is_alive():
        return {
            'code': 102,
            'name': 'Processing'
        }

    thread = threading.Thread(target=main, daemon=True)
    thread.start()

    return {
        'code': 202,
        'name': 'Accepted'
    }


@route('/api/parser/v1/status')
def parser_status():
    response.set_header('Access-Control-Allow-Origin', '*')

    if thread is None:
        return {
            'code': 204,
            'name': 'Not started'
        }

    if thread.is_alive():
        return {
            'code': 102,
            'name': 'Processing'
        }
    
    return {
        'code': 200,
        'name': 'Finished'
    }


if __name__ == '__main__':
    import os

    run(host='0.0.0.0', port=int(os.environ['PORT']))
