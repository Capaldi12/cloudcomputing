from bs4 import BeautifulSoup
import requests
from requests.exceptions import RequestException
from datetime import datetime
import logging
import os

logging.basicConfig()
logger = logging.getLogger('parser_v1')

logger.setLevel(logging.DEBUG)

database_port = 80  # os.environ.get('DATABASE_PORT', 80)
database_url = f'http://database:{database_port}/api/database/articles'

v1 = 'https://v1.ru'

headers = {
    'Accept': 'text/html,application/json,application/xhtml+xml,application/xml;q=0.9,image/webp,image/png,*/*;q=0.8,application/signed-exchange;v=b3;q=0.9',
    'Accept-Encoding': 'gzip, deflate',
    'Accept-Language': 'ru-RU,en-US;q=0.9,en;q=0.8',
    'Dnt': '1',
    'Upgrade-Insecure-Requests': '1',
    'User-agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/70.0.3538.77 Safari/537.36',
    'cookie': '__ddgid=7QOmzEsQQCrRAkij; __ddg2=uveY2ENY13d8kyJB; __ddgid_=5LSAiEV45kKJ3R6y; __ddgmark_=AmRWJr4bTJaWm81e; __ddg2_=uveY2ENY13d8kyJB; __ddg1_=pYP2dPcFHrsOtrkzIobi; jtnews_ab_24=A; jtnews_ab_29=A; ngs_uid=wxPcG2KOOqa0LxAMBMj6Ag==; jtnews_adblock=true; __ddg5_=mgzv9DPDF0pYA3zk; jtnews_top_news={"version":"public_top_news_180","last_visit":1653504525,"current_block_since":1653504503}',
}


def parse_date(date_str: str) -> datetime:
    """
    Парсит строку даты из html в datetime

    :param date_str: строка даты
    :return: datetime представление
    """

    # Удалить ':' потому что питон не умеет его парсить
    if ":" == date_str[-3:-2]:
        date_str = date_str[:-3] + date_str[-2:]

    return datetime.strptime(date_str, "%Y-%m-%dT%H:%M:%S%z")


def get_soup(url: str) -> BeautifulSoup:
    """
    Скачивает страницу по заданному адресу и создаёт для неё "суп"

    :param url: Адрес страницы
    :return: "Суп" страницы
    """

    r = requests.get(url, headers=headers)
    soup = BeautifulSoup(r.content, features="html.parser")

    return soup


def get_links(page: int = None):
    """
    Получает ссылки на новости с заданной страницы списка новостей сайта

    :param page: Номер страницы
    :return: Список ссылок
    """

    logger.info('Parsing links')

    try:

        if page:
            assert page >= 0, 'Page number must be positive'

            # Если страница указана, передаём её как аргумент
            soup = get_soup(v1 + f'/text/?page={page}')
        else:

            # Если нет, просто скачиваем первую страницу
            soup = get_soup(v1 + '/text/')

    except RequestException:
        logger.exception('Error downloading links')
        return None

    # Ищем все теги article. В них первый дочерний тег
    # будет содержать ссылку на статью в аттрибуте href
    links = [next(article.children).attrs['href']
             for article in soup.find_all('article')][::-1]

    # Тут встречаются иногда ссылки на longread - длинные статьи
    # Они работают по другим правилам, поэтому их пока не парсим
    # Ссылки на них даются полные - с адресом сайта и протоколом
    # (в отличие от обычных, которые начинаются с /text)
    # поэтому их легко отфильтровать
    return [v1 + link for link in links if link[:5] == '/text']


def get_article(url: str):
    """
    Парсит статью по заданному адресу и собираем словарь

    :param url: Адрес статьи
    :return: Словарь со статьей
    """

    logger.info(f'Parsing {url}')

    try:

        soup = get_soup(url)

    except RequestException:
        logger.exception('Error downloading page')
        return None

    # На v1 все нужные нам теги имеют аттрибут itemprop
    # Это позволяет быстро найти и выбрать эти теги
    propped = soup.find_all(lambda tag: "itemprop" in tag.attrs)


    # <> datePublished
    # Тут дата публикации вида 2021-04-29T13:00:00+03:00
    # Находится два тега, meta из шапки и какой-то странный div
    # Поэтому сверяю название тега
    date_prop = next(item for item in propped if item.name == 'meta'
                     and item.attrs['itemprop'] == 'datePublished')

    date_published = parse_date(date_prop.attrs['content'])


    # <> headline
    # Заголовок статьи
    headline_prop = next(tag for tag in propped
                         if tag.attrs['itemprop'] == 'headline')

    headline = headline_prop.text


    # <> articleBody
    # Текст статьи. Вроде только сам текст, подписи к картинкам не захватывает
    body_prop = next(tag for tag in propped
                     if tag.attrs['itemprop'] == 'articleBody')

    # Неа, подписи тоже ловим. От них нужно избавиться сначала
    # Судя по всему, весь нужный нам текст сидит конкретно в тегах 'p'
    # Но и весь ненужный тоже. Но у ненужного есть аттрибуты
    # А у нужного их нет. Вроде бы всегда так
    # Поэтому оставляем только те теги, у которых аттрибутов нет
    content = [p.text for p in body_prop.find_all('p')if len(p.attrs) == 0]
    content = '\n'.join(content)

    # Упаковываем все собранные данные в документ и возвращаем
    return {
        'url': url,
        'title': headline,
        'content': content,
        'date': date_published.strftime('%Y-%m-%d')
    }


def parse_from_page(page: int = None):
    """
    Парсит новости с заданной (или первой) страницы сайта

    :param page: Номер страницы
    """

    # Открываем страницу, берем ссылки
    links = get_links(page)

    if links is None:
        return None

    successful = []

    # Парсим все статьи по ссылкам
    for link in links:

        # Парсим страницу статьи
        article = get_article(link)

        if article:
            successful.append(article)

    return successful


def save_article(article):
    r = requests.post(database_url, json=article)

    if r.status_code != 201:
        error = r.json()
        logger.error(f'{error["code"]} {error["name"]}: {error["description"]}')


def main(page=None):
    logger.info('Running parser')

    articles = parse_from_page(page)

    if articles is None:
        logger.error('Cannot parse articles')
        return

    for article in articles:
        save_article(article)


if __name__ == '__main__':
    main()
