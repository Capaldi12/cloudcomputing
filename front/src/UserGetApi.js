import React from 'react'
import { encode } from 'base-64'

class UserGetApi extends React.Component{

    constructor(){
        super();
        this.state = {
            users:null
        }
    }

    loadArticles(){
        fetch('http://localhost:4242/api/database/articles').then((res)=>{
            res.json().then((result)=>{
                //http://localhost:4242/api/database/articles
                //http://jsonplaceholder.typicode.com/users
                var table = document.getElementById("table-articles");
                table.style.display = "block";
                console.warn(result);
                console.warn(result["articles"]);
                this.setState({users:result["articles"]})
            })
        })
    }

    startParserVolgograd(){
        console.log("sus");

        fetch('http://localhost:8080/api/experimental/dags/DAG_VOLGOGRAD_RU/dag_runs',{
            method: 'POST',
            mode: 'no-cors',
            headers: { 'Content-Type': 'application/json', 'accept': 'application/json' },
            body: JSON.stringify({ 'a':'1'})
        }).then(response => response.json());
    }

    startParserV1(){
        fetch('http://localhost:9999/api/parser/v1/run').then((res)=>{
            res.json().then((result)=>{
                console.warn(result);
            })
        })
    }

    render(){
        return(
            <div>
                <div class="row">
                    <div class="col-md-6 offset-md-3">
                        <br /><br />
                        <h3>Get Articles List Using API</h3><br />
                        <button onClick={() => this.loadArticles()}>Load articles</button>
                        <button onClick={() => this.startParserVolgograd()}>Start parse volgograd.ru</button>
                        <button onClick={() => this.startParserV1()}>Start parse v1.ru</button>
                        <table id="table-articles" class="table table-bordered" border="1" style={{display: 'none'}}>
                            <tr>
                                <th>No</th>
                                <th>Title</th>
                                <th>Content</th>
                                <th>Date</th>
                                <th>URL</th>
                            </tr>
                            {
                                this.state.users ?

                                    this.state.users.map((user,i)=>
                                        <tr>
                                            <td>{++i}</td>
                                            <td>{user.title}</td>
                                            <td>{user.content}</td>
                                            <td>{user.date}</td>
                                            <td>{user.url}</td>
                                        </tr>
                                    )
                                    :
                                    null
                            }
                        </table>
                    </div>
                </div>
            </div>
        )
    }
}

export default UserGetApi;