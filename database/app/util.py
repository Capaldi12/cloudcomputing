__all__ = ['mongify', 'demongify',
           'valid_article',
           'created', 'deleted', 'updated']

from flask import jsonify
from bson import ObjectId


def mongify(o):
    try:
        o['_id'] = ObjectId(o['_id'])
    finally:
        return o


def demongify(o):
    try:
        o['_id'] = str(o['_id'])
    finally:
        return o


def valid_article(a: dict):
    required = {'title', 'content', 'url', 'date'}
    permitted = {'_id'}

    actual = set(a.keys())

    if actual & required != required:
        return False

    if actual - required - permitted != set():
        return False

    return True


def created(**fields):
    return fields | {
        'code': 201,
        'name': 'Created',
        'description': 'Successfully created'
    }, 201


def deleted(**fields):
    return fields | {
        'code': 200,
        'name': 'OK',
        'description': 'Successfully deleted'
    }, 200


def updated(**fields):
    return fields | {
        'code': 200,
        'name': 'OK',
        'description': 'Successfully updated'
    }, 200
