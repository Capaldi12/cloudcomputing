"""Docs generation"""

__all__ = ['get_apispec']

from apispec import APISpec
from apispec.ext.marshmallow import MarshmallowPlugin
from apispec_webframeworks.flask import FlaskPlugin


from typing import Iterable


def get_apispec(app, schemas: dict, tags: Iterable[dict]):
    """Get OpenAPI specification for app"""

    spec = APISpec(
        title='Database',
        version='alpha0',
        openapi_version='3.0.3',
        plugins=[FlaskPlugin(), MarshmallowPlugin()],
        info=dict(
            description='API for article database'
        )
    )

    for name, schema in schemas.items():
        spec.components.schema(name, schema=schema)

    for tag in tags:
        spec.tag(tag)

    with app.test_request_context():
        for fn_name in app.view_functions:
            if fn_name == 'static':
                continue

            spec.path(view=app.view_functions[fn_name])

    return spec
