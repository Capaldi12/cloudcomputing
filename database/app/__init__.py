"""Database microservice"""

from flask import Flask
from flask_pymongo import PyMongo

import os

app = Flask('database')

app.config.update(
    MONGO_URI=os.environ.get('CONNECTION_STRING')
)

mongo = PyMongo(app)

from . import routes
