"""Schema definitions"""

__all__ = [
    'ArticleIdSchema',
    'ArticleSchema',
    'ResponseSchema',
    'ResponseArticleListSchema',
    'ResponseArticleSchema',

    'schemas'
]

from marshmallow import Schema, fields, \
    ValidationError, missing

import bson


class ObjectId(fields.String):
    """Custom field to represent bson ObjectID"""

    def _deserialize(self, value, attr, data, **kwargs):
        try:
            return bson.ObjectId(value)
        except Exception:
            raise ValidationError(f'invalid ObjectId `{value}`')

    def _serialize(self, value, attr, obj, **kwargs):
        if value is None:
            return missing
        return str(value)


class ArticleIdSchema(Schema):
    """Article id parameter"""

    article_id = ObjectId(required=True, metadata={
        'description': 'BSON Object ID',
    })


class ArticleSchema(Schema):
    """Represents article in database"""

    _id = ObjectId(metadata={
        'description': 'BSON Object ID [optional]',
        'default': 'Auto generated ID'
    })

    title = fields.Str(required=True, metadata={
        'description': 'Article title',
    })
    url = fields.Url(required=True, metadata={
        'description': 'Article URL',
    })
    content = fields.Str(required=True, metadata={
        'description': 'Article text',
    })
    date = fields.Date(required=True, metadata={
        'description': 'Publication date',
    })


class ResponseSchema(Schema):
    """Basic response schema"""

    code = fields.Int(required=True, metadata={
        'description': 'HTTP response code',
    })
    name = fields.Str(metadata={
        'description': 'HTTP response name [optional]',
    })
    description = fields.Str(metadata={
        'description': 'Readable description [optional]',
    })


class ResponseArticleSchema(ResponseSchema):
    """Schema for article response"""

    article = fields.Nested(ArticleSchema(), metadata={
        'description': 'Requested article',
    })


class ResponseArticleListSchema(ResponseSchema):
    """Schema for articles list response"""

    count = fields.Int(required=True, metadata={
        'description': 'Number of articles returned',
    })
    articles = fields.List(fields.Nested(ArticleSchema()), metadata={
        'description': 'List of articles stored in database',
    })


# Schemes with their names
schemas = {
    "ArticleId": ArticleIdSchema,
    "Article": ArticleSchema,
    "Response": ResponseSchema,
    "ResponseArticle": ResponseArticleSchema,
    "ResponseArticleList": ResponseArticleListSchema,
}
