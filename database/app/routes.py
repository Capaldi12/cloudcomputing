"""Flask routes"""

from flask import abort, request, json, Response
from pymongo.errors import DuplicateKeyError, PyMongoError
from flask_pymongo.wrappers import Collection
from werkzeug.exceptions import HTTPException

from flask_swagger_ui import get_swaggerui_blueprint

from . import app, mongo
from .util import *
from .schema import schemas
from .docs import get_apispec

# Tags used in routes' specs
tags = (
    {'name': 'crud', 'description': 'CRUD operations'},
    {'name': 'list', 'description': 'List all resources'},
)


@app.errorhandler(HTTPException)
def handle_exception(e):
    """Return JSON instead of HTML for HTTP errors."""

    response = e.get_response()
    response.content_type = 'application/json'

    response.data = json.dumps({
        'code': e.code,
        'name': e.name,
        'description': e.description,
    })

    return response


@app.after_request
def add_cors(response: Response):
    """Add CORS header to allow requests form in-browser JS"""

    response.headers['Access-Control-Allow-Origin'] = '*'
    return response


@app.route('/api/database/articles')
def get_articles():
    """
    Get article list

    ---
    get:
      summary: Get article list
      responses:
        200:
          description: "Success: List of articles in database"
          content:
            application/json:
              schema: ResponseArticleListSchema
      tags:
      - list
    """

    article: Collection = mongo.db.article

    articles = [demongify(obj) for obj in article.find()]

    return {
        'code': 200,
        'name': 'OK',
        'count': len(articles),
        'articles': articles
    }, 200


@app.route('/api/database/articles/<article_id>')
def get_article(article_id):
    """
    Get specified article

    ---
    get:
      summary: Get specified article
      parameters:
      - in: path
        name: article_id
        required: true
        schema: ArticleIdSchema
      responses:
        '200':
          description: "Success: Requested article"
          content:
            application/json:
              schema: ResponseArticleSchema
        '404':
          description: "Error: Article not found"
          content:
            application/json:
              schema: ResponseSchema
      tags:
      - crud
    """

    article = mongo.db.article.find_one_or_404(
        mongify({'_id': article_id})
    )

    return {
        'code': 200,
        'name': 'OK',
        'article': demongify(article)
    }, 200


@app.route('/api/database/articles', methods=['POST'])
def create_article():
    """
    Create article with given content

    ---
    post:
      summary: Create article with given content
      parameters:
      - in: body
        name: article
        required: true
        schema: ArticleSchema
      responses:
        '201':
          description: "Success: Article created"
          content:
            application/json:
              schema: ResponseSchema
        '400':
          description: "Error: Empty content"
          content:
            application/json:
              schema: ResponseSchema
        '409':
          description: "Error: Article already exists"
          content:
            application/json:
              schema: ResponseSchema
        '422':
          description: "Error: Invalid content"
          content:
            application/json:
              schema: ResponseSchema
      tags:
      - crud
    """

    if not request.json:
        app.logger.debug('Empty content received')
        abort(400, 'Empty Content')

    article = mongify(request.json)

    if not valid_article(article):
        app.logger.debug(f'Invalid article inserted: {article}')

        abort(422, 'Invalid Article')

    try:
        article_id = mongo.db.article.insert_one(article).inserted_id
        app.logger.debug(f'New article created: {article_id}')

        return created(article_id=str(article_id))

    except DuplicateKeyError:
        app.logger.debug(f'Duplicate key: {article["_id"]}')

        abort(409, 'Already Exists')

    except PyMongoError as e:
        app.logger.exception(f'Exception handling POST: {e}')

        abort(500, 'Unable to save article')


@app.route('/api/database/articles/<article_id>', methods=['PUT'])
def update_article(article_id):
    """
    Update specified article

    ---
    put:
      summary: Update specified article
      parameters:
      - in: path
        name: article_id
        required: true
        schema: ArticleIdSchema
      - in: body
        name: article
        required: true
        schema: ArticleSchema
      responses:
        '200':
          description: "Success: Article updated"
          content:
            application/json:
              schema: ResponseSchema
        '400':
          description: "Error: Empty content"
          content:
            application/json:
              schema: ResponseSchema
        '409':
          description: "Error: Id mismatch"
          content:
            application/json:
              schema: ResponseSchema
        '422':
          description: "Error: Invalid content"
          content:
            application/json:
              schema: ResponseSchema
      tags:
      - crud
    """

    if not request.json:
        app.logger.debug('Empty content received')
        abort(400, 'Empty Content')

    article = mongify(request.json)

    if not valid_article(article):
        app.logger.debug(f'Invalid article updated: {article}')

        abort(422, 'Invalid Article')

    if '_id' in article:
        if article_id != str(article['_id']):
            app.logger.debug(f'Id does not match: '
                             f'{article["_id"]} != {article_id}')

            abort(409, 'Id mismatch')

    else:
        article |= mongify({'_id': article_id})

    try:
        matched = mongo.db.article.replace_one(
            mongify({'_id': article_id}), article).matched_count

        if matched:
            return updated(article_id=article_id)

        else:
            abort(404)

    except PyMongoError as e:
        app.logger.exception(f'Exception handling PUT: {e}')

        abort(500, 'Unable to update article')


@app.route('/api/database/articles/<article_id>', methods=['DELETE'])
def delete_article(article_id):
    """
    Delete specified articles

    ---
    delete:
      summary: Delete specified article
      parameters:
      - in: path
        name: article_id
        required: true
        schema: ArticleIdSchema
      responses:
        '200':
          description: "Success: Article deleted"
          content:
            application/json:
              schema: ResponseSchema
      tags:
      - crud
    """

    try:
        mongo.db.article.delete_one(mongify({'_id': article_id}))

        return deleted(article_id=article_id)

    except PyMongoError as e:
        app.logger.exception(f'Exception handling DELETE: {e}')

        abort(500, 'Unable to delete article')


@app.route('/swagger.json')
def get_swagger_spec():
    """Return spec for swagger"""

    return json.dumps(get_apispec(app, schemas, tags).to_dict())


# Swagger ui at
app.register_blueprint(
    get_swaggerui_blueprint(
        '/api/docs',
        '/swagger.json',
        config={
            'app_name': "database"
        }
    )
)
