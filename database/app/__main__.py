from . import app
from .docs import get_apispec
from .schema import schemas
from .routes import tags

import json

import os
import sys


# Generate documentation
if len(sys.argv) > 1 and sys.argv[1] == '--docs':
    docs_dir = 'docs'
    docs_name = 'database'

    if not os.path.exists(docs_dir):
        os.mkdir(docs_dir)

    spec = get_apispec(app, schemas, tags)

    # JSON
    with open(
        os.path.join(docs_dir, f'{docs_name}.json'), 'w', encoding='utf-8'
    ) as file:

        json.dump(spec.to_dict(), file,
                  indent=2, ensure_ascii=False)

    # YAML
    with open(
        os.path.join(docs_dir, f'{docs_name}.yml'), 'w', encoding='utf-8'
    ) as file:

        file.write(spec.to_yaml())

# Run server
else:
    app.run(host='0.0.0.0', port=int(os.environ.get('PORT', 80)), debug=True)
